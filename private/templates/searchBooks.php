<div class="container">
    <form action="[[ formAction ]]/books/search" method="GET">
        <div class="form-group">
            <input type="text" class="form-control" name="search" placeholder="A book name" value="">
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-default" value="Send"/>
        </div>
    </form>
</div>