<?php
return [
    'Users' => [
        'doLogout' => 100,
        'unregister' => 100,
        'edit' => 100,
        'update' => 100,
        'allUsers' => 200,
        'editCurrent' => 100,
        'create' => 200,

    ],
    'Books' => [
        'index' => 100,
        'all' => 1,
        'search' => 1,
        'details' => 1,
        'by' => 1
    ],
    'Bookings' => [
        'index' => 200,
        'booking' => 100,
        'by' => 100,
        'byUserEmail' => 100,
        'currentUser' => 100,
        'byBookId' => 100,
        'return' => 200
    ],
    'BooksParamethers' => [
        'index' => 300
    ]
];