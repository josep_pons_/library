<?php

return [
    'non-member' => 1,
    'member' => 100,
    'librarian' => 200,
    'root' => 300
];