/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50717
Source Host           : localhost:3306
Source Database       : library_db

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2017-06-07 16:00:11
*/

SET FOREIGN_KEY_CHECKS=0;


-- ----------------------------
-- Table structure for books
-- ----------------------------
DROP TABLE IF EXISTS `books`;
CREATE TABLE `books` (
  `id` varchar(255) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of books
-- ----------------------------
INSERT INTO `books` VALUES ('BdhXh1QnU1cC', 'El gato negro y otros cuentos', 'Edgar Allan Poe', '');
INSERT INTO `books` VALUES ('cUIzMQAACAAJ', 'Doce años de Esclavitud', 'Solomon Northup', '\"Doce a�os de esclavitud\" -la narrativa sobrecogedora y reveladora que inspir� el largometraje que gan� el premio Oscar como mejor pel�cula del a�o- es la obra de Solomon Northup, un afroamericano libre que fue secuestrado en el capitolio del pa�s y vendido como esclavo en Luisiana. Es uno de los relatos m�s convincente y detallado que tenemos de c�mo era de verdad la esclavitud. Redactada despu�s de que Northup hab�a pasado doce a�os esclavizado, la narrativa -en vez de representar al autor como un h�roe- le concede al lector un relato extraordinario de la comunidad esclava. Puesto que era un hombre educado que fue arrancado de la libertad, Northup era capaz de observar las atrocidades de la esclavitud desde un punto de vista �nico y logr� describirlas de manera espantosa y v�vida. Con una claridad imparable, se centra en la vida y los trabajos de los esclavos de las plantaciones del sur estadounidense preb�lico, y examina las complejas decisiones econ�micas de la posesi�n de esclavos, as� como tambi�n la manera calamitosa que la instituci�n de la esclavitud afect� la base de la civilizaci�n. Inteligente, apasionada, y absolutamente �nica en la literatura estadounidense, \"Doce a�os de esclavitud\" es una contribuci�n inestimable a la literatura, ahora disponible en espa�ol. \"Junto con, La caba�a de t�o Tom, la narrativa sobrecogedora de Solomon Northup es la obra m�s destacada que ha salido de la prensa estadounidense.\" - Detroit Tribune (1853)');
INSERT INTO `books` VALUES ('f6xEwTtZ3TgC', 'Moby Dick', 'Herman Melville', 'Ishmael, a sailor, recounts the ill-fated voyage of a whaling ship led by the fanatical Captain Ahab in search of the white whale that had crippled him. Presented in comic book format.');
INSERT INTO `books` VALUES ('hm3NrQEACAAJ', 'Los juegos del hambre', 'Suzanne Collins', 'Science fiction. 16-årige Kattua lever i en fremtid, hvor der skal udvælges et ungt menneske, der skal kæmpe til døden mod andre udvalgte. Da Kattuas lillesøster bliver udvalgt, melder hun sig som frivillig og overtager søsterens plads i det årlige dødsspil');
INSERT INTO `books` VALUES ('MzARPAAACAAJ', 'El Afgano', 'Frederick Forsyth', 'Mike Martin, un veterano de guerra de cuarenta y cuatro años, se ha jubilado de los cuerpos especiales de élite del ejército británico. Ahora se dedica a restaurar la vieja casa de campo que ha comprado y lleva una vida apacible y feliz. Pero Mike, además de una notable hoja de servicios, posee unas interesantes características. Su padre trabajaba en una empresa petrolera y Mike fue educado en Irak. Por otra parte, sus facciones no son propiamente británicas. Los rasgos y el tono de piel, heredados de su abuela india, no le distinguían de sus compañeros de escuela y puede pasar por un iraquí o, más exactamente, por un afgano. De ahí que los servicios de inteligencia británicos se interesen por él. Los últimos y graves acontecimientos políticos -los atentados a las Torres Gemelas y en el metro de Londres- han aumentado la necesidad de captar y descifrar todo mensaje en árabe que circule en la red. Todos los recursos son pocos para intentar evitar un nuevo atentado terrorista. Los expertos afirman que algo muy grave se está tramando, pero son incapaces de predecir cómo, dónde y cuándo. Ahí entra en escena Mike. El plan es enormemente arriesgado. Mike es instruido durante meses en la cultura y el idioma afgano. Se trata de darle una nueva identidad, la de Izmat Jan, un guerrillero afgano que lleva cinco años encarcelado en Guantánamo. Tras varios meses de preparación, el gobierno americano anuncia que Izmat Jan será deportado a su país. Así es como Mike es lanzado al terreno enemigo. Mike consigue enviar mensajes a sus superiores. Solo de ese modo es posible que estos consigan abortar los planes de la organización terrorista. Pero ¿serán capaces de rescatar a Mike del corazón del infierno?');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_type` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`,`email`),
  KEY `email` (`email`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'admin', 'admin@admin.com', '$2y$10$x2k0PNx2mO7P29RR/QlAuep/zJqzUj/GiWi46Nb4AaBBF6MfZUutK', 'root');
INSERT INTO `users` VALUES ('2', 'erik', 'erik@iesjoanramis.org', '$2y$10$x2k0PNx2mO7P29RR/QlAuep/zJqzUj/GiWi46Nb4AaBBF6MfZUutK', 'member');
INSERT INTO `users` VALUES ('9', 'victor', 'vgomez@iesjoanramis.org', '$2y$10$LX98pMmPhhCWDZO3IwqC7.T2OeYF0R5uG6HYRhwvwEx5zxPZGe9B2', 'member');
INSERT INTO `users` VALUES ('16', 'Groot', 'tt@tt.com', '$2y$10$474uq3A4InbXmdp1ERelieropIHKuI3D513NWkYKbREAw/N6Isa9C', 'root');
INSERT INTO `users` VALUES ('17', 'victor gomez', 'somebody@someplace.com', '$2y$10$K5sP4aOCuJHTXtCAa42pwOBcQS0u06UlKeWblKv2lAsLBbHqxdZHC', 'member');
INSERT INTO `users` VALUES ('18', 'josep pons', 'jponspons@gmail.com', '$2y$10$ZQX4RTmPB..rAoQ9QQz5hOLGyonXvT5Q6URZ.Zevnx9a6WIgObVSa', 'root');


-- ----------------------------
-- Table structure for bookings
-- ----------------------------
DROP TABLE IF EXISTS `bookings`;
CREATE TABLE `bookings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `book_id` varchar(255) NOT NULL,
  `user_email` VARCHAR(100) COLLATE utf8_unicode_ci NOT NULL,
  `pick_up` varchar(255) NOT NULL,
  `pick_off` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `booking_book` (`book_id`),
  KEY `booking_user` (`user_email`),
  CONSTRAINT `booking_book` FOREIGN KEY (`book_id`) REFERENCES `books` (`id`),
  CONSTRAINT `booking_user` FOREIGN KEY (`user_email`) REFERENCES `users` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bookings
-- ----------------------------
INSERT INTO `bookings` VALUES ('2', 'BdhXh1QnU1cC', 'jponspons@gmail.com', '06/07/2017', '06/14/2017');
INSERT INTO `bookings` VALUES ('3', 'BdhXh1QnU1cC', 'jponspons@gmail.com', '06/24/2017', '06/30/2017');
INSERT INTO `bookings` VALUES ('4', 'BdhXh1QnU1cC', 'jponspons@gmail.com', '07/08/2017', '07/22/2017');
